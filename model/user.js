// const bcrypt = require("bcrypt");
const mongoose = require("mongoose");
const constants = require("../helpers/constants");
const userSchema = new mongoose.Schema({
  f_name: {
    type: String,
    trim: true,
  },
  l_name: {
    type: String,
    trim: true,
  },
  profilePic: String,
  email: {
    type: String,
    unique: true,
  },
  password: {
    type: String,
    unique: true,
    timestamps: true,
  },

  status: Number,

  user: {
    type: String,
    default: "admin",
    enum: ["admin", "manager"],
    required: true,
  },
});

const User = mongoose.model("User", userSchema);

module.exports = User;
