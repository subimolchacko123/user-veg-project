// const bcrypt = require("bcrypt");
const mongoose = require("mongoose");

const vegSchema = new mongoose.Schema({
  veg_name: {
    type: String,
    trim: true,
    required: true,
  },
  color: {
    type: String,
    required: true,
  },

  price: {
    type: Number,
    required: true,
  },
  creatorId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
});

const Vegetable = mongoose.model("Vegetable", vegSchema);

module.exports = Vegetable;
