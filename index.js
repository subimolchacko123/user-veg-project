const express = require("express");
const dotenv = require("dotenv");
const mongoose = require("mongoose");

const bodyParser = require("body-parser");

//Routes

const productRoutes = require("./routes/product.route");
const adminRoutes = require("./routes/admin.route");

//For .env
dotenv.config({
  path: ".env",
});

//express initialisation
const app = express();

//PORT
const PORT = process.env.PORT || 5000;

/**
 * Connect to MongoDB.
 */

mongoose.connect(process.env.MONGODB_URI);
mongoose.connection.on("error", (err) => {
  console.error(err);
  console.log(
    "%s MongoDB connection error. Please make sure MongoDB is running."
  );
  process.exit();
});

app.use(express.json());

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

/**
 * Express configuration.
 */

app.set("host", process.env.OPENSHIFT_NODEJS_IP || "0.0.0.0");
app.set("port", process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8081);

// Middlewares

//Auth Routes
// app.use("/auth", authRoutes);

//Admin Routes
app.use("/admin", adminRoutes);
app.use("/api", productRoutes);
//PORT Running
app.listen(PORT, () => {
  console.log(`App is running at http://localhost:${PORT}`);
});
