const express = require("express");
const router = express.Router();
const upload = require("../middleware/fileUpload");
const auth = require("../middleware/adminAuth");
const adminAccounts = require("../controllers/admin.controller");
//Accounts
router.post("/accounts/register", upload, adminAccounts.registerAdmin);
router.post("/accounts/login", adminAccounts.login);
router.get("/accounts/lists", auth, adminAccounts.listUsers);
router.get("/accounts/search", auth, adminAccounts.getUserBySearch);
router.patch("/accounts/update", auth, adminAccounts.updateUser);
router.delete("/accounts/delete/:id", auth, adminAccounts.deleteUser);

module.exports = router;
