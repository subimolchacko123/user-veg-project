const express = require("express");
const router = express.Router();
const auth = require("../middleware/commonAuth");
const productAccounts = require("../controllers/product.controller");
//Accounts
router.post("/product/add", auth, productAccounts.productAdd);
router.get("/product/list", auth, productAccounts.productList);
router.patch("/product/update", auth, productAccounts.productUpdate);
router.delete("/product/delete/:id", auth, productAccounts.productDelete);
router.get("/product/singledetails", auth, productAccounts.getSingleProduct);

module.exports = router;
