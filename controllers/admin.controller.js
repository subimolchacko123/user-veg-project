var User = require("../model/user");
const jwt = require("jsonwebtoken");
var constants = require("../helpers/constants");
var bcrypt = require("bcryptjs");
var paramsConfig = require("../config/params.config");
// var config = require("../../config/app.config.js");
// var usersConfig = config.userPage;
const JWT_KEY = paramsConfig.key.jwt.secret;
const salt = bcrypt.genSaltSync(10);

// USER MANAGEMENT API

this.registerAdmin = async (req, res) => {
  let params = req.body;

  if (!params.f_name || !params.email || !params.password) {
    var msg = "";
    if (!params.f_name) {
      msg = "First name cannot be empty";
    } else if (!params.email) {
      msg = "Email cannot be empty";
    } else if (!params.password) {
      msg = "password cannot be empty";
    }
    return res.send({
      msg,
      statusCode: 400,
      error: true,
      data: null,
    });
  }

  var findCriteria = {
    email: params.email,
    status: 1,
  };

  let emailCheck = await User.findOne(findCriteria).catch((error) => {
    console.log(error);
    return {
      msg: "Something went wrong while check email",
      statusCode: 400,
      error: true,
      data: null,
    };
  });
  if (emailCheck && emailCheck.error !== undefined && emailCheck.error) {
    return res.send(emailCheck);
  }
  if (emailCheck) {
    return res.send({
      msg: "Email already exists",
      statusCode: 400,
      error: true,
      data: null,
    });
  }
  var pswd = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
  );
  var isPaswd = pswd.test(params.password);
  if (isPaswd) {
    var hash = bcrypt.hashSync(params.password, salt);
  } else {
    return res.send({
      msg: "Enter a strong password",
      statusCode: 400,
      error: true,
      data: null,
    });
  }
  // if (params.user === constants.MANAGER) {
  //   user = params.user;
  // } else {
  //   return res.send({
  //     msg: "please select user type as manager",
  //     statusCode: 400,
  //     error: true,
  //     data: null,
  //   });
  // }

  const AdminRegistration = new User({
    f_name: params.f_name,
    l_name: params.l_name,
    user: params.user,
    email: params.email,
    password: hash,
    profilePic: req.files[0].filename,
    status: 1,
  });

  let createData = await AdminRegistration.save().catch((error) => {
    console.log(error);
    return {
      msg: "Something went wrong while register Admin",
      statusCode: 400,
      error: true,
      data: null,
    };
  });
  if (createData && createData.error !== undefined && createData.error) {
    return res.send(createData);
  }
  return res.send({
    msg: "Admin registration successfull...",
    id: createData.id,
    statusCode: 200,
    error: false,
  });
};
// Login
this.login = async (req, res) => {
  let params = req.body;
  if (!params.email || !params.password) {
    if (!params.email) {
      return res.send({
        msg: "email cannot be empty",
        statusCode: 400,
        error: true,
        data: null,
      });
    }
    if (!params.password) {
      return res.send({
        msg: "password cannot be empty",
        statusCode: 400,
        error: true,
        data: null,
      });
    }
  }
  var patt = new RegExp("^([A-Za-z0-9_.-]+)@([da-z.-]+).([a-z.]{2,6})$");
  var isEmail = patt.test(params.email);

  if (isEmail) {
    var findCriteria = {
      status: 1,
      email: params.email,
    };
  }

  let adminCheck = await User.findOne(findCriteria).catch((error) => {
    console.log(error);
    return {
      msg: "Something went wrong while getting user",
      statusCode: 400,
      error: true,
      data: null,
    };
  });
  console.log(adminCheck);

  if (adminCheck && adminCheck.error !== undefined && adminCheck.error) {
    return res.send(adminCheck);
  }

  if (adminCheck) {
    let matched = await bcrypt.compare(params.password, adminCheck.password);
    //console.log(matched);
    if (matched) {
      var payload = {
        id: adminCheck.id,
        user: adminCheck.user,
      };
      var jwtToken = jwt.sign(
        {
          data: payload,
        },
        JWT_KEY,
        {
          expiresIn: "10h",
        }
      );
      console.log("JWT_KEY : " + JWT_KEY);
      let data = {
        f_ame: adminCheck.f_name,
        email: adminCheck.email,
        jwtToken,
      };
      return res.send({
        data,
        msg: "Login successfull",
        statusCode: 200,
        error: false,
      });
    } else {
      return res.send({
        msg: "Incorrect password",
        statusCode: 400,
        error: true,
        data: null,
      });
    }
  } else {
    return res.send({
      msg: "Incorrect email",
      statusCode: 400,
      error: true,
      data: null,
    });
  }
};

//List all users and sort with fields
this.listUsers = async (req, res) => {
  per_page = parseInt(req.query.limit) || 20;
  page_no = parseInt(req.query.page) || 1;
  var pagination = {
    limit: per_page,
    skip: per_page * (page_no - 1),
  };

  let findCriteria = {};
  let project = {
    password: 0,
  };
  var AllUsers = await User.find(findCriteria, project)
    .sort({
      f_name: 1,
      l_name: 1,
      user: 1,
      email: 1,
    })
    .limit(pagination.limit)
    .skip(pagination.skip)
    .exec()

    .catch((error) => {
      console.log(error);
      return {
        msg: "Something went wrong while checking User data",
        statusCode: 400,
        error: true,
        data: error,
      };
    });
  if (AllUsers && AllUsers.error !== undefined && AllUsers.error) {
    return res.send(AllUsers);
  }
  if (AllUsers) {
    return res.send({
      msg: "User fetch susccessfully",
      data: AllUsers,
      statusCode: 200,
      error: false,
    });
  } else {
    return res.send({
      msg: "No User",
      statusCode: 400,
      error: true,
      data: null,
    });
  }
};
//search with fields

this.getUserBySearch = async (req, res) => {
  var params = req.query;
  per_page = parseInt(req.query.limit) || 10;
  page_no = parseInt(req.query.page) || 1;
  var pagination = {
    limit: per_page,
    skip: per_page * (page_no - 1),
  };
  let findCriteria = {
    $or: [
      { f_name: { $regex: params.searchText, $options: "i" }, status: 1 },
      { l_name: { $regex: params.searchText, $options: "i" }, status: 1 },
      { email: { $regex: params.searchText, $options: "i" }, status: 1 },
      { user: { $regex: params.searchText, $options: "i" }, status: 1 },
    ],
  };
  var project = {
    f_name: 1,
    l_name: 1,
    user: 1,
    email: 1,
    profilePic: 1,
  };
  var userList = await User.find(findCriteria, project)
    .limit(pagination.limit)
    .skip(pagination.skip)
    .exec()
    .catch((error) => {
      console.log(error);
      return {
        msg: "Something went wrong while list user",
        statusCode: 400,
        error: true,
        data: null,
      };
    });
  if (userList && userList.error !== undefined && userList.error) {
    return res.send(userList);
  }

  return res.send({
    data: userList,
    msg: "User list",
    statusCode: 200,
    error: false,
  });
};

this.updateUser = async (req, res) => {
  let params = req.body;
  console.log(req.body);
  // console.log(id);
  if (!params.id) {
    return res.send({
      msg: "Id Can not be empty",
      statusCode: 400,
      error: true,
      data: null,
    });
  }

  let userDetails = {};

  if (params.f_name) {
    userDetails.f_name = params.f_name;
  }
  if (params.l_name) {
    userDetails.l_name = params.l_name;
  }
  if (params.email) {
    userDetails.email = params.email;
  }
  if (params.user) {
    userDetails.user = params.user;
  }
  if (params.image) {
    userDetails.profilePic = params.image;
  }
  if (params.status) {
    userDetails.status = params.status;
  }

  let findCriteria = { _id: params.id };
  let project = {
    password: 0,
  };

  let createdCustomer = await User.findOneAndUpdate(
    findCriteria,
    userDetails
  ).catch((error) => {
    console.log(error);
    return res.send({
      msg: "Something went wrong while Updating User",
      statusCode: 400,
      error: true,
      data: null,
    });
  });

  return res.send({
    msg: "User successfully Updated",
    statusCode: 200,
    error: false,
    data: createdCustomer,
  });
};
//delete user
this.deleteUser = async (req, res) => {
  var userId = req.params.id;
  console.log(userId);
  if (!userId) {
    var msg = "";
    if (!userId) {
      msg = "id cannot be empty";
    }
    return res.send({
      msg,
      statusCode: 400,
      error: true,
      data: null,
    });
  }

  var userData = await User.deleteOne({ _id: userId }).catch((error) => {
    console.log(error);
    return {
      msg: "Something went wrong while checking Customer data",
      statusCode: 400,
      error: true,
      data: null,
    };
  });
  if (userData && userData.error !== undefined && userData.error) {
    return res.send(userData);
  }
  if (userData) {
    return res.send({
      msg: "User Details Delete susccessfully",
      data: userData,
      statusCode: 200,
      error: false,
    });
  } else {
    return res.send({
      msg: "No User Found",
      statusCode: 400,
      error: true,
      data: null,
    });
  }
};
