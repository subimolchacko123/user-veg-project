var User = require("../model/user");
var bcrypt = require("bcryptjs");

var Vegetable = require("../model/vegetable");
// PRODUCT API

this.productAdd = async (req, res) => {
  let params = req.body;
  let creatorId = params.creatorId;
  if (!params.veg_name || !params.color || !params.price) {
    var msg = "";
    if (!params.veg_name) {
      msg = "Name cannot be empty";
    } else if (!params.color) {
      msg = "Color cannot be empty";
    } else if (!params.price) {
      msg = "price cannot be empty";
    }
    return res.send({
      msg,
      statusCode: 400,
      error: true,
      data: null,
    });
  }
  var hexacode = new RegExp("#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$");
  var isHexacode = hexacode.test(params.color);
  if (isHexacode) {
    validcolor = params.color;
  } else {
    return res.send({
      msg: "Enter Valid Color Format",
      statusCode: 400,
      error: true,
      data: null,
    });
  }

  var numericvalue = new RegExp(/^\d{1,10}(\.\d{1,4})?$/);
  var numeric = numericvalue.test(params.price);

  if (numeric) {
    validprice = params.price;
  } else {
    // validprice = params.price;
    // console.log("hai");
    // console.log(validprice);
    return res.send({
      msg: "Enter Valid Price Format",
      statusCode: 400,
      error: true,
      data: null,
    });
  }

  const vegDetails = new Vegetable({
    veg_name: params.veg_name,
    color: validcolor,
    price: validprice,
    creatorId: creatorId,
  });
  // console.log(vegDetails);
  let createData = await vegDetails.save().catch((error) => {
    console.log(error);
    return {
      msg: "Something went wrong while add vegetable details",
      statusCode: 400,
      error: true,
      data: null,
    };
  });
  if (createData && createData.error !== undefined && createData.error) {
    return res.send(createData);
  }
  return res.send({
    msg: "Vegetable added successfull...",
    id: createData.id,
    statusCode: 200,
    error: false,
  });
};
//List all vegitables
this.productList = async (req, res) => {
  per_page = parseInt(req.query.limit) || 10;
  page_no = parseInt(req.query.page) || 1;
  var pagination = {
    limit: per_page,
    skip: per_page * (page_no - 1),
  };
  var AllVegitables = await Vegetable.find()
    .limit(pagination.limit)
    .skip(pagination.skip)
    .exec()

    .catch((error) => {
      console.log(error);
      return {
        msg: "Something went wrong while checking Vegetable data",
        statusCode: 400,
        error: true,
        data: error,
      };
    });
  if (
    AllVegitables &&
    AllVegitables.error !== undefined &&
    AllVegitables.error
  ) {
    return res.send(AllVegitables);
  }
  if (AllVegitables) {
    return res.send({
      msg: "Vegetables fetch susccessfully",
      data: AllVegitables,
      statusCode: 200,
      error: false,
    });
  } else {
    return res.send({
      msg: "No Vegitables",
      statusCode: 400,
      error: true,
      data: null,
    });
  }
};

//update vegetables
this.productUpdate = async (req, res) => {
  let params = req.body;
  decRegx = params.color;
  console.log(req.body);
  // console.log(id);
  if (!params.id) {
    return res.send({
      msg: "Id Can not be empty",
      statusCode: 400,
      error: true,
      data: null,
    });
  }

  var hexacode = new RegExp("#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$");
  var isHexacode = hexacode.test(params.color);
  if (isHexacode) {
    validcolor = params.color;
  } else {
    return res.send({
      msg: "Enter Valid Color Format",
      statusCode: 400,
      error: true,
      data: null,
    });
  }

  var numericvalue = new RegExp(/^\d{1,10}(\.\d{1,4})?$/);
  var numeric = numericvalue.test(params.price);

  if (numeric) {
    validprice = params.price;
  } else {
    return res.send({
      msg: "Enter Valid Price Format",
      statusCode: 400,
      error: true,
      data: null,
    });
  }

  let productDetails = {};

  if (params.veg_name) {
    productDetails.veg_name = params.veg_name;
  }
  if (validcolor) {
    productDetails.color = validcolor;
  }
  if (validprice) {
    productDetails.price = validprice;
  }

  let findCriteria = { _id: params.id };

  let createdVegDetails = await Vegetable.findOneAndUpdate(
    findCriteria,
    productDetails
  ).catch((error) => {
    console.log(error);
    return res.send({
      msg: "Something went wrong while Updating Vegetables",
      statusCode: 400,
      error: true,
      data: null,
    });
  });
  return res.send({
    msg: "Veg successfully Updated",
    statusCode: 200,
    error: false,
    data: createdVegDetails,
  });
};
//delete vegitables
this.productDelete = async (req, res) => {
  var vegId = req.params.id;
  //   console.log(vegId);
  if (!vegId) {
    var msg = "";
    if (!vegId) {
      msg = "id cannot be empty";
    }
    return res.send({
      msg,
      statusCode: 400,
      error: true,
      data: null,
    });
  }

  var vegData = await Vegetable.deleteOne({ _id: vegId }).catch((error) => {
    console.log(error);
    return {
      msg: "Something went wrong while checking Vegetable data",
      statusCode: 400,
      error: true,
      data: null,
    };
  });
  if (vegData && vegData.error !== undefined && vegData.error) {
    return res.send(vegData);
  }
  if (vegData) {
    return res.send({
      msg: "Veg Details Delete susccessfully",
      data: vegData,
      statusCode: 200,
      error: false,
    });
  } else {
    return res.send({
      msg: "No Vegitables Found",
      statusCode: 400,
      error: true,
      data: null,
    });
  }
};
// get veg detail with particular id
this.getSingleProduct = async (req, res) => {
  var params = req.query;
  //   console.log(params.id);
  if (!params.id) {
    var msg = "";
    if (!params.id) {
      msg = "id cannot be empty";
    }
    return res.send({
      msg,
      statusCode: 400,
      error: true,
      data: null,
    });
  }

  let findCriteria = { _id: params.id };

  var productDetails = await Vegetable.findOne(findCriteria).populate(
    "creatorId",
    {
      f_name: 1,
    }
  );

  if (productDetails) {
    return res.send({
      msg: "Vegetables fetch susccessfully",
      data: productDetails,
      statusCode: 200,
      error: false,
    });
  } else {
    return res.send({
      msg: "No vegetables ",
      statusCode: 400,
      error: true,
      data: null,
    });
  }
};
