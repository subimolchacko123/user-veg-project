// const upload = require("../models/user");
const multer = require("multer");
const path = require("path");
const uuid = require("uuid");

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, path.join(__dirname, "../public/adminupload/"));
  },
  filename(req, file, cb) {
    cb(null, uuid.v4() + path.extname(file.originalname));
  },
});
// const upload = multer({ storage });s
var uploadimage = multer({ storage: storage }).array("image", 1);

module.exports = uploadimage;
