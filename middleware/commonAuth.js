const jwt = require("jsonwebtoken");
var User = require("../model/user");
// var config = require('../../config/app.config.js');
var paramsConfig = require("../config/params.config");
const constants = require("../helpers/constants");
const { MANAGER } = require("../helpers/constants");

const JWT_KEY = paramsConfig.key.jwt.secret;

const auth = async (req, res, next) => {
  try {
    // console.log('haai');
    // const token = req.header('Authorization').trim();
    // const token = req.headers.authorization.split(" ")[1];

    const token = req.header("Authorization").replace("Bearer ", "");

    const userDetails = jwt.verify(token, JWT_KEY);

    const data = userDetails.data;
    const userId = data.id;
    //console.log(data);

    const user = await User.findOne({
      _id: userId,
      status: 1,
    });
    if (user.user === constants.ADMIN || user.user === constants.MANAGER) {
      if (!user) {
        throw new Error();
      }
      req.user = user;
      req.token = token;
      next();
    }
    // console.log(user);
  } catch (error) {
    console.log("error");
    console.log(error);
    console.log("error");
    res.status(401).send({ error: "Not authorized to access this resource" });
  }
};
module.exports = auth;
